# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2
#
# TODO(b/133112560): Delete this package once there are no more references.
# All ebuilds that referenced this should be updated to reference the new
# sys-apps/etphidiap name directly.

EAPI="6"

DESCRIPTION="Old name for the sys-apps/etphidiap package."

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND="sys-apps/etphidiap"
RDEPEND="sys-apps/etphidiap"
