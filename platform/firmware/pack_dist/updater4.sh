#!/bin/sh
#
# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# For factory and auto update, after shell-ball self-extracts, this script is
# called to update BIOS and EC firmware as per how many files are extracted.
# To simply design, THIS SCRIPT MUST BE EXECUTED IN A R/W EXCLUSIVE TEMP FOLDER.
# AND ALL FILENAMES FOR INPUT AND OUTPUT MUST NOT CONTAIN SPACE.
#
# Allowed commands are:
# - standard shell commands
# - flashrom
# - futility
# - crossystem
# All other special commands should be defined by a function in crosutil.sh.
#
# Temporary files should be named as "_*" to prevent confliction

# Updater for firmware v4 (two-stop main, chromeos-ec).
# This is designed for x86/arm platform, using chromeos-ec (software sync).
# Assume SLOT_A and SLOT_B has exactly same contents (and same keyblock).

SCRIPT_BASE="$(dirname "$0")"
. "$SCRIPT_BASE/common.sh"

# Parse command line
FLAGS "$@" || exit 1
eval set -- "$FLAGS_ARGV"

# Use bundled tools with highest priority, to prevent dependency when updating
cros_setup_path

# This setup must occur before anything else since the unibuild
# variables need to be set before the rest of the initialization.
if [ -n "${UNIBUILD}" ]; then
  model="${FLAGS_model}"
  if [ "${FLAGS_mode}" = "output" ]; then
    [ -n "${model}" ] || die \
      "Model (--model) must be specified with --mode=output"
    [ -n "${FLAGS_output_dir}" ] || die \
      "Output directory (--output_dir) must be specified with --mode=output"
  fi

  if [ -z "${model}" ]; then
    model="$(mosys platform model)"
    [ -n "${model}" ] || die "Cannot get model from mosys."
  fi
  # Image and key files were set in the shared crosfw.sh file. Set them again
  # here, since we want to use the files specific to the chosen model.
  cros_set_unibuild_vars "${model}"
fi


# ----------------------------------------------------------------------------
# Customization Section

# Customization script file name - do not change this.
# You have to create a file with this name to put your customization.
CUSTOMIZATION_SCRIPT="updater_custom.sh"

# Customization script main entry - do not change this.
# You have to define a function with this name to run your customization.
CUSTOMIZATION_MAIN="updater_custom_main"

# ----------------------------------------------------------------------------
# Constants

# Slot names defined by ChromeOS Firmware Specification
SLOT_A="RW_SECTION_A"
SLOT_B="RW_SECTION_B"
SLOT_RO="RO_SECTION"
SLOT_RW_SHARED="RW_SHARED"
SLOT_LEGACY="RW_LEGACY"
SLOT_EC_RO="EC_RO"
SLOT_EC_RW="EC_RW"
SLOT_PD_RO="EC_RO"
SLOT_PD_RW="EC_RW"

# Main = Application (AP) or SoC firmware, sometimes considered as "BIOS"
TYPE_MAIN="main"
# EC = Embedded Controller
TYPE_EC="ec"
# PD = Power Delivery
TYPE_PD="pd"

# ----------------------------------------------------------------------------
# Global Variables

# Current system identifiers (may be empty if running on non-ChromeOS systems)
HWID="$(crossystem hwid 2>/dev/null)" || HWID=""

# Compare following values with TARGET_*
# (should be passed by wrapper as environment variables)
FWID="$(crossystem fwid 2>/dev/null)" || FWID=""
RO_FWID="$(crossystem ro_fwid 2>/dev/null)" || RO_FWID=""
PLATFORM="${FWID%%.*}"

# Determine EC version
SYSFS_CROS_EC="/sys/class/chromeos/cros_ec/version"
if [ -e "${SYSFS_CROS_EC}" ]; then
  ECID="$(sed -n 's/^RW version: *//p' ${SYSFS_CROS_EC})"
else
  ECINFO="$(mosys -k ec info 2>/dev/null)" || ECINFO=""
  ECID="$(eval "$ECINFO"; echo "$fw_version")"
fi

# Determine PD version
SYSFS_CROS_PD="/sys/class/chromeos/cros_pd/version"
if [ -e "${SYSFS_CROS_PD}" ]; then
  PDID="$(sed -n 's/^RW version: *//p' ${SYSFS_CROS_PD})"
else
  PDINFO="$(mosys -k pd info 2>/dev/null)" || PDINFO=""
  PDID="$(eval "$PDINFO"; echo "$fw_version")"
fi

# ----------------------------------------------------------------------------
# Helper functions

# Verifies if current system is installed with compatible rootkeys
check_compatible_keys() {
  local current_image="$DIR_CURRENT/$IMAGE_MAIN"
  local target_image="$DIR_TARGET/$IMAGE_MAIN"
  if [ "${FLAGS_check_keys}" = ${FLAGS_FALSE} ]; then
    debug_msg "check_compatible_keys: ignored."
    return $FLAGS_TRUE
  fi
  if ! cros_check_same_root_keys "$current_image" "$target_image"; then
    alert_incompatible_rootkey
    die "Incompatible Rootkey."
  fi

  # Get RW firmware information
  local fw_info
  fw_info="$(cros_get_rw_firmware_info "$DIR_TARGET/$TYPE_MAIN/VBLOCK_A" \
                                       "$DIR_TARGET/$TYPE_MAIN/FW_MAIN_A" \
                                       "$target_image")" || fw_info=""
  [ -n "$fw_info" ] || die "Failed to get RW firmware information"

  # Check TPM
  if ! cros_check_tpm_key_version "$fw_info"; then
    alert_incompatible_tpmkey
    die "Incompatible TPM Key."
  fi

  # Warn for RO-normal updates
  local flag_ro_normal_boot=1
  local current_flags="$(cros_get_firmware_preamble_flags "$fw_info")"
  if [ "$((current_flags & flag_ro_normal_boot))" = "$flag_ro_normal_boot" ]
  then
    alert "
    WARNING: FIRMWARE IMAGE TO BE UPDATED IS SIGNED WITH 'RO-NORMAL' FLAG.
    THIS IS A KEY-BLOCK-ONLY UPDATE WITHOUT FIRMWARE CODE CHANGE.
    YOUR FWID (ACTIVE FIRMWARE ID) WON'T CHANGE AFTER APPLYING THIS UPDATE.
    "
  fi
}

need_update_legacy() {
  debug_msg "Checking whether legacy update is needed..."
  if [ "${FLAGS_force}" = ${FLAGS_FALSE} ] && \
     crosfw_is_equal_slot "${TYPE_MAIN}" "${SLOT_LEGACY}"; then
    debug_msg "${SLOT_LEGACY} not changed."
    return "${FLAGS_FALSE}"
  fi

  local sentinel_name='cros_allow_auto_update'
  local current_file=".legacy.current"
  local target_file=".legacy.target"
  rm -f "${current_file}" "${target_file}"

  # cbfstool may not exist on certain old systems. Try our best shot.
  cbfstool "${DIR_CURRENT}/${TYPE_MAIN}/${SLOT_LEGACY}" extract \
    -n "${sentinel_name}" -f "${current_file}" >/dev/null 2>&1 || true
  cbfstool "${DIR_TARGET}/${TYPE_MAIN}/${SLOT_LEGACY}" extract \
    -n "${sentinel_name}" -f "${target_file}" >/dev/null 2>&1 || true

  # We should update only if both target and current CBFS allows update.
  if ! [ -f "${target_file}" ]; then
    debug_msg "cbfstool failure or target not for auto update. Ignore."
    return "${FLAGS_FALSE}"
  fi
  if ! [ -f "${current_file}" ]; then
    debug_msg "cbfstool failure or old/custom legacy firmware. Ignore update."
    return "${FLAGS_FALSE}"
  fi
  return "${FLAGS_TRUE}"
}

check_and_update_legacy() {
  if need_update_legacy; then
    verbose_msg "${SLOT_LEGACY} is changed and will be updated."
    crosfw_update_main "${SLOT_LEGACY}"
  fi
}

need_update_main_ro() {
  debug_msg "Checking need_update_main_ro"
  if [ "${TARGET_RO_FWID}" != "${RO_FWID}" ]; then
    debug_msg "TARGET_RO_FWID != RO_FWID."
    return ${FLAGS_TRUE}
  fi

  prepare_main_image
  prepare_main_current_image

  cp -f "${DIR_CURRENT}/${TYPE_MAIN}/${SLOT_RO}" .ro_from
  cp -f "${DIR_TARGET}/${TYPE_MAIN}/${SLOT_RO}" .ro_to
  cp -f "${DIR_CURRENT}/${TYPE_MAIN}/GBB" .gbb_from
  cp -f "${DIR_TARGET}/${TYPE_MAIN}/GBB" .gbb_to

  # HWID should be already preserved.
  local file
  for file in .ro_from .ro_to .gbb_from .gbb_to; do
    futility gbb -s --flags=0 "${file}" >/dev/null 2>&1
  done

  if ! cros_compare_file .gbb_from .gbb_to; then
    verbose_msg "RO GBB Contents (probably root keys) are different."
    return $FLAGS_TRUE
  fi

  if ! cros_compare_file .ro_from .ro_to; then
    verbose_msg "RO Contents are different."
    return $FLAGS_TRUE
  fi
  return $FLAGS_FALSE
}

need_update_main() {
  if [ "$TARGET_FWID" != "$FWID" ]; then
    return $FLAGS_TRUE
  fi

  prepare_main_image
  prepare_main_current_image

  if is_vboot2; then
    # Slot A and B are not guaranteed to be the same with vboot2, and we do not
    # always boot out of slot A.  This check must first determine what the
    # active slot is and compare that instead of always comparing A to B.
    local mainfw_act="$(cros_get_prop mainfw_act)"
    local vblock=""

    if [ "$mainfw_act" = "A" ]; then
      vblock="VBLOCK_A"
    elif [ "$mainfw_act" = "B" ]; then
      vblock="VBLOCK_B"
    else
      die "autoupdate: unexpected active firmware ($_mainfw_act)..."
    fi

    # Compare VBLOCK from current and target slot (normal firmware).
    if ! crosfw_is_equal_slot "$TYPE_MAIN" "$vblock" "$vblock"; then
      debug_msg "$vblock differs between current and target"
      return $FLAGS_TRUE
    fi
  else
    # Compare VBLOCK from current A slot and target B slot (normal firmware).
    if ! crosfw_is_equal_slot "$TYPE_MAIN" "VBLOCK_A" "VBLOCK_B"; then
      debug_msg "VBLOCK in A and B differ"
      return $FLAGS_TRUE
    fi
  fi
}

prepare_main_image() {
  crosfw_unpack_image "$TYPE_MAIN" "$IMAGE_MAIN" "$TARGET_OPT_MAIN"
}

prepare_main_current_image() {
  # Reading main current image is slow so we do want to cache.
  local cached_file="${DIR_CURRENT}/.cached"
  if [ -e "${cached_file}" ]; then
    debug_msg "prepare_main_current_image: Use existing cache."
  else
    crosfw_unpack_current_image "$TYPE_MAIN" "$IMAGE_MAIN" "$TARGET_OPT_MAIN"
    touch "${cached_file}"
  fi
}

is_write_protection_disabled() {
  if [ "${FLAGS_update_main}" = ${FLAGS_TRUE} ]; then
    is_mainfw_write_protected && return $FLAGS_FALSE || true
  fi

  if [ "${FLAGS_update_ec}" = ${FLAGS_TRUE} ]; then
    is_ecfw_write_protected && return $FLAGS_FALSE || true
  fi

  if [ "${FLAGS_update_pd}" = ${FLAGS_TRUE} ]; then
    is_pdfw_write_protected && return $FLAGS_FALSE || true
  fi

  return $FLAGS_TRUE
}

clear_update_cookies() {
  # Always success because the system may not have crossystem ready yet if we're
  # trying to recover a broken firmware or after transition from legacy firmware
  ( cros_set_fwb_tries 0
    cros_set_startup_update_tries 0
    cros_set_prop recovery_request=0 ) >/dev/null 2>&1 ||
      debug_msg "clear_update_cookies: there were some errors, ignored."
}

silent_sh() {
  # Calls given commands and ignores any error (mostly for factory
  # installations, when the firmware is still non-Chrome).
  ( "$@" ) >/dev/null 2>&1 ||
    debug_msg "Failed calling: $@"
}

enable_dev_boot() {
  cros_set_prop dev_boot_usb=1 dev_boot_signed_only=0
}

disable_dev_boot() {
  # The firmware will decide and reset default values of dev_boot_usb and
  # dev_boot_signed_only on reoot when user turned off developer switch (i.e.,
  # normal mode). It's safe to set dev_boot_usb to zero here, but
  # dev_boot_signed_only may expect different default values, so we leave it
  # untouched and let firmware decide.
  cros_set_prop dev_boot_usb=0
}

load_keyset() {
  if ! [ -d "$KEYSET_DIR" ]; then
    debug_msg "No keysets folder."
    return
  fi
  local keyid="" whitelabel_tag=""

  # Allow overriding keys from command line.
  # TODO(hungte) In future if cros_config is available on ALL (including legacy)
  # systems, we can consider again moving all these logic to cros_config.
  if [ -n "${FLAGS_signature_id}" ]; then
    keyid="${FLAGS_signature_id}"
  elif [ -n "${FLAGS_customization_id}" ]; then
    # TODO(hungte) Remove FLAGS_customization_id in future.
    # Match how VPD customization_id is processed below.
    keyid="${FLAGS_customization_id%%-*}"
  else
    # For Unified build, the keyset is use by each model and white-label.
    # For Non-Unified build, keyset is used only for white-label devices.
    # Legacy devices use 'customization_id' (upper cased) while newer devices
    # use 'whitelabel_tag' (lower cased).
    if [ -n "${UNIBUILD}" ]; then
      local model="$(mosys platform model)"
      debug_msg "Unified Build: Default signature=${SIGNATURE_ID}"
      # TODO(hungte) Search some white-label-specific property in cros_config
      # to make sure this is a white label device.
      keyid="${SIGNATURE_ID##sig-id-in-*}"
      if [ -n "${SIGNATURE_ID}" ] && [ -z "${keyid}" ]; then
        debug_msg "Trying to find signature from VPD 'whitelabel_tag'"
        whitelabel_tag="$(vpd -g whitelabel_tag 2>/dev/null || true)"
        if [ -n "${whitelabel_tag}" ]; then
          keyid="${model}-${whitelabel_tag}"
        else
          # This is for RMA, early builds or factory first time installation.
          # TODO(hungte) Only allow this in special condition, for example if
          # write protection is disabled.
          debug_msg "Fallback to model of Unified Build."
          keyid="${model}"
        fi
      fi
    else
      # Non-Unified Build. Find 'whitelabel_tag' or 'customization_id'.
      debug_msg "Trying to find signature from VPD 'whitelabel_tag'"
      whitelabel_tag="$(vpd -g whitelabel_tag 2>/dev/null || true)"
      if [ -n "${whitelabel_tag}" ]; then
        # The signerbot is simply using keyset names (UPPERCASED) in non-unified
        # build so we have to convert whitelabel_tag.
        # In some environments like netboot initramfs, the `tr` is provided by
        # busybox and do not support POSIX names like :lower: and :upper:, so we
        # have to use [a-z] explicitly.
        keyid="$(echo "${whitelabel_tag}"| tr '[a-z]' '[A-Z]')"
      else
        # `customization_id` comes in format LOEM-SERIES and we have to drop
        # [-SERIES] for finding right signed files.
        debug_msg "Trying to find signature from VPD 'customization_id'"
        local customization_id="$(vpd -g customization_id)"
        keyid="${customization_id%%-*}"
      fi
    fi
  fi

  if [ -z "${keyid}" ]; then
    if [ -n "${UNIBUILD}" ]; then
      die "Unibuild: No signature ID in VPD/cmdline. Cannot continue."
    else
      verbose_msg "No customization_id. Use default keys."
    fi
    return
  fi

  debug_msg "Keysets detected, using [$keyid]"
  local rootkey="$KEYSET_DIR/rootkey.$keyid"
  local vblock_a="$KEYSET_DIR/vblock_A.$keyid"
  local vblock_b="$KEYSET_DIR/vblock_B.$keyid"
  if ! [ -s "$rootkey" ]; then
    die "Failed to load keysets for signature [$keyid]."
  fi
  # Override keys
  futility gbb -s --rootkey="$rootkey" "$IMAGE_MAIN" ||
    die "Failed to update rootkey from keyset [$keyid]."
  local size_input="$(cros_get_file_size "$IMAGE_MAIN")"
  local param="dummy:emulate=VARIABLE_SIZE,image=$IMAGE_MAIN,size=$size_input"
  silent_invoke flashrom -p "$param" -w "$IMAGE_MAIN" \
    -i "VBLOCK_A:$vblock_a" -i "VBLOCK_B:$vblock_b" ||
    die "Failed to update VBLOCK from keyset [$keyid]."
  # TODO(hungte) Verify key correctness after building new image files.
  verbose_msg "Firmware keys changed to set [$keyid]."
}

is_vboot2() {
  crossystem 'fw_vboot2?1'
}

# ----------------------------------------------------------------------------
# Core logic in different modes

# For unified builds that support separate root/fw keys per model,
# this option allows creation of a targeted model specific bios/ec image, which
# can then be used to automate the factory imaging process without requiring
# an firmware install specific logic.
mode_output() {
  cp "${IMAGE_MAIN}" "${FLAGS_output_dir}"
  cp "${IMAGE_EC}" "${FLAGS_output_dir}"
  verbose_msg "Firmware images generated to: ${FLAGS_output_dir}"
}

# Startup
mode_startup() {
  # ChromeOS-EC do not need to be updated at startup time.
  cros_set_startup_update_tries 0
}

# Update Engine - Received Update
mode_autoupdate() {
  if [ "${FLAGS_update_main}" = "${FLAGS_FALSE}" ]; then
    echo "No main firmware to upload, abort."
    return 1
  fi

  if ! is_mainfw_write_protected && need_update_main_ro; then
    mode_recovery
    return
  fi

  # Quick check if we need to update
  if [ "${FLAGS_force}" = ${FLAGS_TRUE} ] || need_update_main; then
    verbose_msg "Firmware update available: ${TARGET_FWID}. "
  else
    verbose_msg "Latest RW firmware already installed. No need to update."
    check_and_update_legacy
    return
  fi

  local mainfw_act="$(cros_get_prop mainfw_act)"
  local update_slot="${SLOT_B}"
  local prop_name=fwb_tries
  local try_next=

  if is_vboot2; then
    if [ "$mainfw_act" = "A" ]; then
      update_slot="$SLOT_B"
      try_next=B
    elif [ "$mainfw_act" = "B" ]; then
      update_slot="$SLOT_A"
      try_next=A
    else
      die "autoupdate: unexpected active firmware ($_mainfw_act)..."
    fi
    prop_name=fw_try_count
  else  # vboot1
    if [ "$mainfw_act" = "B" ]; then
      # mainfw_act is only updated at system reboot; if two updates (both with
      # firmware updates) are pushed in a row, next update will be executed
      # while mainfw_act is still B. Since we don't use RW BIOS directly when
      # updater is running, it should be safe to update in this case.
      debug_msg "mainfw_act=B, checking if we can still update FW B..."
      prepare_main_current_image
      cros_compare_file "$DIR_CURRENT/$TYPE_MAIN/$SLOT_A" \
                        "$DIR_CURRENT/$TYPE_MAIN/$SLOT_B" &&
        alert "Installing updates while mainfw_act is B (should be safe)." ||
        die_need_reboot "Done (retry update next boot)"
    elif [ "$mainfw_act" != "A" ]; then
      die "autoupdate: unexpected active firmware ($mainfw_act)..."
    fi
  fi

  prepare_main_image
  prepare_main_current_image
  check_compatible_keys
  crosfw_update_main "$update_slot"
  check_and_update_legacy

  if [ -n "$try_next" ]; then
    cros_set_prop "fw_try_next=$try_next"
  fi

  # Try to determine EC software sync by checking $ECID. We can't rely on
  # $TARGET_ECID, $FLAGS_update_ec or $IMAGE_EC because in future there may be
  # no EC binary blobs in updater (integrated inside main firmware image).
  # Note since $ECID uses mosys, devices not using ChromeOS EC may still
  # report an ID (but they should use updater v3 instead).
  if [ -n "$ECID" ] && [ "$ECID" != "$TARGET_ECID" ]; then
    # EC software sync may need extra reboots.
    cros_set_prop "${prop_name}=8"
    verbose_msg "On reboot EC update may occur."
  else
    cros_set_prop "${prop_name}=6"
  fi
}

# Recovery Installer
mode_recovery() {
  local prefix="recovery"
  [ "${FLAGS_mode}" = "recovery" ] || prefix="${FLAGS_mode}(recovery)"
  if [ "${FLAGS_update_main}" = ${FLAGS_TRUE} ]; then
    if ! is_mainfw_write_protected; then
      # TODO(hungte) check and alert if the original mode is autoupdate, and TPM
      # rollback detected.
      verbose_msg "$prefix: update RO+RW"
      crosfw_preserve_vpd
      crosfw_preserve_gbb
      crosfw_preserve_nvram
      crosfw_update_main
    else
      # TODO(hungte) check if FMAP is not changed
      verbose_msg "$prefix: update main/RW:A,B,SHARED,LEGACY"
      prepare_main_image
      prepare_main_current_image
      check_compatible_keys
      crosfw_update_main "$SLOT_A"
      crosfw_update_main "$SLOT_B"
      crosfw_update_main "$SLOT_RW_SHARED"
      # Some old platforms like Daisy may not have SLOT_LEGACY.
      if [ -f "${DIR_TARGET}/${TYPE_MAIN}/${SLOT_LEGACY}" ]; then
        crosfw_update_main "$SLOT_LEGACY"
      fi
    fi
  fi

  if [ "${FLAGS_update_ec}" = ${FLAGS_TRUE} ]; then
    if ! is_ecfw_write_protected; then
      verbose_msg "$prefix: update ec/RO+RW"
      crosfw_update_ec
    fi
    # If EC is write-protected, software sync will lock RW EC after kernel
    # starts (left firmware boot stage). We can't "recovery" EC RW in this case.
    # Ref: crosbug.com/p/16087.
    verbose_msg "$prefix: EC may be restored or updated in next boot."
  fi

  if [ "${FLAGS_update_pd}" = ${FLAGS_TRUE} ]; then
    if ! is_pdfw_write_protected; then
      verbose_msg "$prefix: update pd/RO+RW"
      crosfw_update_pd
    fi
    verbose_msg "$prefix: PD may be restored or updated in next boot."
  fi

  clear_update_cookies
}

# Factory Installer
mode_factory_install() {
  # Everything executed here must assume the system may be not using ChromeOS
  # firmware.
  is_write_protection_disabled ||
    die_need_ro_update "You need to first disable hardware write protection."

  if [ "${FLAGS_update_main}" = ${FLAGS_TRUE} ]; then
    # We may preserve bitmap here, just like recovery mode. However if there's
    # some issue (or incompatible stuff) found in bitmap, we will need a method
    # to update the bitmaps.
    crosfw_preserve_vpd || verbose_msg "Warning: cannot preserve VPD."
    crosfw_update_main
  fi
  if [ "${FLAGS_update_ec}" = ${FLAGS_TRUE} ]; then
    crosfw_update_ec
  fi
  if [ "${FLAGS_update_pd}" = ${FLAGS_TRUE} ]; then
    crosfw_update_pd
  fi
  cros_clear_nvdata
  silent_sh enable_dev_boot
  clear_update_cookies
}

# Force update RW_LEGACY
mode_legacy() {
  echo "Updating ${SLOT_LEGACY} firmware..."
  crosfw_update_main "${SLOT_LEGACY}"
}

# ----------------------------------------------------------------------------
# Main Entry

main() {
  cros_acquire_lock
  set_flags

  # factory compatibility
  if [ "${FLAGS_factory}" = ${FLAGS_TRUE} ] ||
     [ "${FLAGS_mode}" = "factory" ]; then
    FLAGS_mode=factory_install
  fi
  local ro_type=""
  cros_is_ro_normal_boot && ro_type="$ro_type[RO_NORMAL]"

  verbose_msg "Starting $TARGET_PLATFORM firmware updater v4 (${FLAGS_mode})..."
  local package_info="$TARGET_FWID"
  if [ "$TARGET_FWID" != "$TARGET_RO_FWID" ]; then
    package_info="RO:$TARGET_RO_FWID RW:$TARGET_FWID"
  fi
  local current_info="RO:$RO_FWID $ro_type, ACT:$FWID"
  if [ -n "${TARGET_ECID}" ]; then
    package_info="$package_info / EC:$TARGET_ECID"
  fi
  if [ -n "$ECID" ]; then
    current_info="$current_info / EC:$ECID"
  fi
  if [ -n "${TARGET_PDID}" ]; then
    package_info="$package_info / PD:$TARGET_PDID"
  fi
  if [ -n "$PDID" ]; then
    current_info="$current_info / PD:$PDID"
  fi
  verbose_msg " - Updater package: [$package_info]"
  verbose_msg " - Current system:  [$current_info]"

  if [ "${FLAGS_update_main}" = ${FLAGS_TRUE} -a -n "${HWID}" ]; then
    # always preserve HWID for current system, if available.
    crosfw_preserve_hwid
    debug_msg "preserved HWID as: $HWID."
  fi

  local wpmsg="$(cros_report_wp_status $FLAGS_update_main \
                 $FLAGS_update_ec $FLAGS_update_pd)"
  verbose_msg " - Write protection: $wpmsg"

  # Check platform except in factory_install mode.
  if [ "${FLAGS_check_platform}" = ${FLAGS_TRUE} ] &&
     [ "${FLAGS_mode}" != "factory_install" ] &&
     [ "${FLAGS_mode}" != "output" ] &&
     ! cros_check_compatible_platform "${TARGET_PLATFORM}" "${PLATFORM}"; then
    alert_unknown_platform "$PLATFORM" "$TARGET_PLATFORM"
    exit 1
  fi

  # Since CL:798773, all additional files are in bin/.
  # TODO(hungte) Remove this hack when we have separated extra programs and data.
  if [ ! -r "${CUSTOMIZATION_SCRIPT}" ] && [ -r "bin/${CUSTOMIZATION_SCRIPT}" ]
  then
    CUSTOMIZATION_SCRIPT="bin/${CUSTOMIZATION_SCRIPT}"
  fi

  # load customization
  if [ -r "$CUSTOMIZATION_SCRIPT" ]; then
    debug_msg "loading customization..."
    . ./$CUSTOMIZATION_SCRIPT
    # invoke customization
    debug_msg "starting customized updater main..."
    $CUSTOMIZATION_MAIN
  fi

  load_keyset

  result="completed"
  case "${FLAGS_mode}" in
    autoupdate | recovery | startup | factory_install | output | legacy)
      mode_"${FLAGS_mode}" || result="failed"
      ;;

    "" )
      die "Please assign updater mode by --mode option."
      ;;

    * )
      die "Unknown mode: ${FLAGS_mode}"
      ;;
  esac
  verbose_msg "Firmware update (${FLAGS_mode}) ${result}."
}

# Exit on error
set -e

# Main Entry
main
