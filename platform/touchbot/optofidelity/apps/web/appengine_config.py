# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from google.appengine.ext import vendor

# Add any libraries installed in the "lib" folder.
vendor.add('lib')