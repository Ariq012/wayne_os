开发者选项
显示调试信息
启用操作系统验证
关机
语言
从网络启动
启动 Legacy BIOS
从 USB 启动
从 USB 或 SD 卡启动
从内部磁盘启动
取消
确认启用操作系统验证
停用操作系统验证
确认停用操作系统验证
使用音量按钮可上移/下移至不同的选项
使用电源按钮可选择选项。
停用操作系统验证功能会使您的系统处于不安全状态。
选择“取消”可保持受保护状态。
操作系统验证功能已关闭。您的系统目前处于不安全状态。
选择“启用操作系统验证”可恢复安全状态。
选择“确认启用操作系统验证”可保护您的系统。
