# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from fuzzy_check import FuzzyCheck
from validators import *

# originally generated gestures:
# Motion d=1030 x=1029 y=28.45 r=2.13

def Validate(raw, events, gestures):
  fuzzy = FuzzyCheck()
  fuzzy.expected = [
    MotionValidator(roughness="< 0.8 ~ 0.2"),
  ]
  fuzzy.unexpected = [
  ]
  return fuzzy.Check(gestures)
